function StartLoop()
{
    TransitionToMainMenu();
    requestAnimationFrame(MainLoop);
}

var previousTime = 0.0;
var lag = 0.0;

var MS_PER_UPDATE = 16.67;

var fallingSpeed = 0;
var rotation = 0;
var currentY = 45;

var velocityOnTap = -2;
var gravityIncrement = 0.1;

var lastSpawnedUpperPipe = null;
var offsetForNewPipe = 200;
var distanceBetweenPipes = 0.2;

var hasHitFloor = false;

var score = 0;
var upperPipesAddedToScore = [];

function MainLoop(Time)
{
    var current = Time;
    if (previousTime === 0.0)
    {
        previousTime = current;
    }

    var elapsed = current - previousTime;

    previousTime = current;

    lag += elapsed;

    while (lag >= MS_PER_UPDATE)
    {
        update();
        lag -= MS_PER_UPDATE;
    }

    draw();
    requestAnimationFrame(MainLoop);
}

function update()
{
    if (currentState === GameState.MainMenu)
    {
        AnimateBackground();
    }
    else if (currentState === GameState.PreGame)
    {
        AnimateBackground();
    }
    else if (currentState === GameState.InGame)
    {
            AnimateBackground();
            ApplyGravity();
            UpdatePipes();
            MovePipes();
            CheckPipesPassed();
    }
    else if (currentState === GameState.GameOver)
    {
        ApplyGravity();
    }
}

function draw()
{
    if (currentState === GameState.PreGame)
    {
        DrawScore();
    }
    else if (currentState === GameState.InGame)
    {
        RotateBird();
        MoveBird();
        DrawScore();
    }
    else if (currentState === GameState.GameOver)
    {
        if (!hasHitFloor)
        {
            RotateBird();
            MoveBird();
        }
    }
}

function TransitionToGameover()
{
    $("#flappybird_ingame").css("-webkit-animation", "none");
    $("#flappybird_ingame").css("-moz-animation", "none");
    $("#flappybird_ingame").css("-ms-animation", "none");
    $("#flappybird_ingame").css("animation", "none");

    $("#gameover").show();

    $("#whitescreen").fadeIn(50).fadeOut(50);

    $("#gameover_text").fadeIn(700, ShowScoreboard);
    currentState = GameState.GameOver;
}

function ShowScoreboard()
{
    $("#scoreboard").fadeIn(700, 'linear', EnableOkBtn);
}

function EnableOkBtn()
{
    $("#ok_button").show();
}

function TransitionToInGame()
{
    $("#main_menu").fadeOut(500, 'linear', StartGame);
    $("#background").fadeOut(500, 'linear');
}

function StartGame()
{
    currentY = 45;
    hasHitFloor = false;

    document.getElementById("ingame").addEventListener("touchstart", function(event)
    {
        tap();
        event.preventDefault();
    });

    $("#ingame").mousedown(function(event)
    {
        tap();
    });

    $("#ingame").fadeIn(500, 'linear');
    $("#background").fadeIn(500, 'linear');
    currentState = GameState.PreGame;
}

function tap()
{
    if (currentState === GameState.PreGame)
    {
        $("#ingame_start").fadeOut(900, 'linear');
        currentState = GameState.InGame;
    }

    fallingSpeed = velocityOnTap;

    rotation = -45;
}

function TransitionToMainMenu()
{
    if (currentState === GameState.GameOver)
    {
        $("#ingame").fadeOut(500, 'linear', InitMainMenu);
        $("#gameover").fadeOut(500, 'linear');
        $("#background").fadeOut(500, 'linear');
    }
    else
    {
        InitMainMenu();
    }
}

function InitMainMenu()
{
    score = 0;

    var shallowCopyLowerPipe = $.extend({}, $(".lower_pipe"));
    $.each(shallowCopyLowerPipe, function(idx, item)
    {
        item.remove();
    });

    var shallowCopyUpperPipe = $.extend({}, $(".upper_pipe"));
    $.each(shallowCopyUpperPipe, function(idx, item)
    {
        item.remove();
    });

    $("#firstdigit").removeAttr('style');
    $("#seconddigit").removeAttr('style');
    $("#ingame_points_container").removeAttr('style');
    $("#whitescreen").removeAttr('style');
    $("#flappybird_ingame").removeAttr('style');
    $("#ingame_start").removeAttr('style');
    $("#ok_button").removeAttr('style');
    $("#scoreboard").removeAttr('style');
    $("#gameover_text").removeAttr('style');

    $("#background").fadeIn(500, 'linear');
    $("#main_menu").fadeIn(500, 'linear');
    currentState = GameState.MainMenu;
}

function IsColliding()
{
    var bird = $("#flappybird_ingame");
    var height = bird.height() * Math.abs(Math.cos(rotation)) + bird.width() * Math.abs(Math.sin(rotation));

    if(IsCollidingWithFloor())
    {
        return true;
    }

    var collided = false;
    $(".lower_pipe").each(function()
    {
        var left = $(this).offset().left;

        var right = left + $(this).width();
        var top = $(this).offset().top;

        if ((bird.offset().left + bird.width()) > left && bird.offset().left < right && (bird.offset().top + height) > top)
        {
            collided = true;
        }
    });

    if (collided === true)
    {
        return true;
    }

    $(".upper_pipe").each(function()
    {
        var left = $(this).offset().left;
        var right = left + $(this).width();
        var bottom = $(this).height();

        if ((bird.offset().left + bird.width()) > left && bird.offset().left < right && bird.offset().top < bottom)
        {
            collided = true;
        }
    });

    if (collided === true)
    {
        return true;
    }

    return false;
}

function IsCollidingWithFloor()
{
    var bird = $("#flappybird_ingame");
    var height = bird.height() * Math.abs(Math.cos(rotation)) + bird.width() * Math.abs(Math.sin(rotation));

    if((bird.offset().top + height) >=
        ($("#viewport").height() - $(".scrolling_background:first").height()))
    {
        return true;
    }
}

function AnimateBackground()
{
    var backgroundPos = $(".scrolling_background:first").css('background-position').split(" ");
    var backgroundX = parseInt(backgroundPos[0]);

    if (backgroundX <= -387)
    {
        $(".scrolling_background").each(function()
        {
           $(this).css('background-position', '-276px -410px');
        });
    }
    else
    {
        backgroundX -= 3;
        $(".scrolling_background").each(function()
        {
            $(this).css('background-position', '' + (backgroundX) + 'px -410px');
        });
    }
}

function ApplyGravity()
{
    fallingSpeed += gravityIncrement;

    if (rotation < 90)
    {
        rotation += 2;
    }
}

function MoveBird()
{
    currentY += fallingSpeed;
    if (currentY < 0)
        currentY = 0;

    $("#flappybird_ingame").css('top', '' + currentY + '%');

    if (IsColliding() && currentState !== GameState.GameOver)
    {
        if (IsCollidingWithFloor() && !hasHitFloor)
        {
            rotation = 90;
            RotateBird();

            hasHitFloor = true;
            var bird = $("#flappybird_ingame");
            var height = bird.width() * Math.abs(Math.cos(rotation)) + bird.height() * Math.abs(Math.sin(rotation));

            var birdAtFloor = $("#viewport").height() - $(".scrolling_background:first").height() + (bird.width() - bird.height()) - 5 - bird.width();

            bird.css('top', '' + (birdAtFloor) + 'px');
        }

        TransitionToGameover();
    }
    else if (IsCollidingWithFloor() && currentState === GameState.GameOver && !hasHitFloor)
    {
        rotation = 90;
        RotateBird();

        hasHitFloor = true;

        var bird = $("#flappybird_ingame");
        var height = bird.height() * Math.abs(Math.cos(rotation)) + bird.width() * Math.abs(Math.sin(rotation));

        var birdAtFloor = $("#viewport").height() - $(".scrolling_background:first").height() + (bird.width() - bird.height()) - 5 - bird.width();

        bird.css('top', '' + birdAtFloor + 'px');
    }
}

function RotateBird()
{
    $("#flappybird_ingame").css({'-webkit-transform' : 'rotate('+ rotation +'deg)',
        '-moz-transform' : 'rotate('+ rotation +'deg)',
        '-ms-transform' : 'rotate('+ rotation +'deg)',
        'transform' : 'rotate('+ rotation +'deg)'});
}

function DrawScore()
{
    if (score <= 99 && score >= 0)
    {

        var firstDigit = parseInt(score / 10);
        var secondDigit = score % 10;

        if (firstDigit > 0)
        {
            $("#firstdigit").show();
            SetNumber($("#firstdigit"), firstDigit)
        }

        SetNumber($("#seconddigit"), secondDigit);
    }
}

function SetNumber(element, number)
{
    switch(number)
    {
        case 0:
            element.width(14);
            element.height(27);
            element.css('background-position', '0 -370px');
            break;
        case 1:
            element.width(14);
            element.height(27);
            element.css('background-position', '-17px -370px');
            break;
        case 2:
            element.width(15);
            element.height(27);
            element.css('background-position', '-31px -370px');
            break;
        case 3:
            element.width(14);
            element.height(27);
            element.css('background-position', '-48px -370px');
            break;
        case 4:
            element.width(14);
            element.height(27);
            element.css('background-position', '-64px -370px');
            break;
        case 5:

            element.width(14);
            element.height(27);
            element.css('background-position', '-80px -370px');
            break;
        case 6:

            element.width(14);
            element.height(27);
            element.css('background-position', '-96px -370px');
            break;
        case 7:

            element.width(14);
            element.height(27);
            element.css('background-position', '-112px -370px');
            break;
        case 8:

            element.width(14);
            element.height(27);
            element.css('background-position', '-128px -370px');
            break;
        case 9:

            element.width(14);
            element.height(27);
            element.css('background-position', '-144px -370px');
            break;
    }
}

function UpdatePipes()
{
    var shallowCopy = $.extend({}, $(".upper_pipe"));
    $.each(shallowCopy, function(idx, item)
    {
        if ($(this).offset().left < (-$(this).width()))
        {
            item.remove();
        }
    });

    var shallowCopy = $.extend({}, $(".lower_pipe"));
    $.each(shallowCopy, function(idx, item)
    {
        if ($(this).offset().left < (-$(this).width()))
        {
            item.remove();
        }
    });

    if (lastSpawnedUpperPipe === null ||
        lastSpawnedUpperPipe.offset().left < ($(window).width() - offsetForNewPipe))
    {
        lastSpawnedUpperPipe = CreateNewUpperPipe();
        var lowerPipe = CreateNewLowerPipe();

        var lowerBounds = $(window).height() - $("#scrolling_background_container").height() - 100 - (distanceBetweenPipes * $(window).height());
        var upperHeight = Math.random() * lowerBounds + 50;

        lastSpawnedUpperPipe.height(upperHeight);
        lastSpawnedUpperPipe.css("background-position", "-552px " + (-410 + upperHeight) + "px");

        var lowerPos = upperHeight + (distanceBetweenPipes * $(window).height());
        lowerPipe.offset().top = lowerPos;

        var lowerHeight = $("#scrolling_background_container").offset().top - lowerPos;
        lowerPipe.height(lowerHeight);

    }
}

function MovePipes()
{
    $(".lower_pipe").each(function()
    {
        var currTrans = $(this).css('transform').split(/[()]/)[1];
        var posx = currTrans.split(',')[4];

        $(this).css('transform', 'translateX(' + (posx - 3) + 'px)');
    });

    $(".upper_pipe").each(function()
    {
        var currTrans = $(this).css('transform').split(/[()]/)[1];
        var posx = currTrans.split(',')[4];

        $(this).css('transform', 'translateX(' + (posx - 3) + 'px)');
    });
}

function CheckPipesPassed()
{
    $(".upper_pipe").each(function()
    {
        if (!ListContains(upperPipesAddedToScore, $(this)))
        {

            var rightEdge = $(this).offset().left + $(this).width();
            if (rightEdge < $("#flappybird_ingame").offset().left)
            {
                upperPipesAddedToScore.push($(this));
                score++;
            }
        }
    });
}

function ListContains(list, element)
{
    var hasElement = false;

    $(list).each(function()
    {
       if ($(this).is(element))
       {
           hasElement = true;
       }
    });

    return hasElement;
}

function CreateNewUpperPipe()
{
    var upperPipe = $("<div></div>");
    upperPipe.addClass("upper_pipe");
    $("#ingame").append(upperPipe);
    return upperPipe;
}

function CreateNewLowerPipe()
{
    var lowerPipe = $("<div></div>");
    lowerPipe.addClass("lower_pipe");
    $("#ingame").append(lowerPipe);
    return lowerPipe;
}