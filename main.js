var GameState = {
    MainMenu: 0,
    PreGame: 1,
    InGame: 2,
    GameOver: 3
};

var currentState = GameState.MainMenu;

$(document).ready(function()
{
    currentState = GameState.MainMenu;
    Initialize();
});

function Initialize()
{
    DuplicateCityBackgroundAlongWidth();
    DuplicateSlidingBackgroundAlongWidth();

    window.onresize = function()
    {
        DuplicateCityBackgroundAlongWidth();
        DuplicateSlidingBackgroundAlongWidth();
    };

    $("#background").hide();
    StartLoop();
}

function DuplicateCityBackgroundAlongWidth()
{
    var shallowCopy = $.extend({}, $(".city_background"));
    $.each(shallowCopy, function(idx, item)
    {
        if (idx > 0)
        {
            item.remove();
        }
    });

    var viewPortWidth = $("#viewport").width();
    var cityBackgroundWidth = $(".city_background:first").width();

    while (viewPortWidth >= ($("#city_background_container").width() + cityBackgroundWidth))
    {
        $(".city_background:first").clone().appendTo("#city_background_container");
    }

    var remainingWidth = viewPortWidth - $("#city_background_container").width();
    var lastBackground = $(".city_background:first").clone();

    lastBackground.width(remainingWidth);
    lastBackground.appendTo("#city_background_container");
}

function DuplicateSlidingBackgroundAlongWidth()
{
    var shallowCopy = $.extend({}, $(".scrolling_background"));
    $.each(shallowCopy, function(idx, item)
    {
        if (idx > 0)
        {
            item.remove();
        }
    });

    var viewPortWidth = $("#viewport").width();
    var cityBackgroundWidth = $(".scrolling_background:first").width();

    while (viewPortWidth >= ($("#scrolling_background_container").width() + cityBackgroundWidth))
    {
        $(".scrolling_background:first").clone().appendTo("#scrolling_background_container");
    }

    var remainingWidth = viewPortWidth - $("#scrolling_background_container").width();
    var lastBackground = $(".scrolling_background:first").clone();

    lastBackground.width(remainingWidth);
    lastBackground.appendTo("#scrolling_background_container");
}